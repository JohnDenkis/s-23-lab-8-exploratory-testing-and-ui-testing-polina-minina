# Exploratory testing

There is description of tests of "Papa Johns" cite https://papajohns.ru

Legend:
* ✅ - correct
* ⛔ - bug
* 💡 - open question

Actual result can be empty if it is same as Expected result.

## Test 1 - Unregistered user cart 

| Test tour           	| CART-1                       	|
|---------------------	|----------------------------	|
| Object to be tested 	| Creating cart being initially unregistered 	|
| Test duration       	| 50 secs                    	|
| Tester              	| Polina Minina              	|

### Protocol

| # 	| Steps                                                     	| Status 	| Expected result                                                  	| Actual result  	|
|---	|-----------------------------------------------------------	|--------	|------------------------------------------------------------------	|----------------	|
| 1 	| Go to main page as unregistered user.                     	| ✅      	| Main page is opened and user displayed are unregistered.         	|                	|
| 2 	| Add two pizza to the cart.                                	| ✅      	| Two selected pizza are displayed in the cart.                    	|                	|
| 3 	| Click to the button "SIGN IN".                            	| ✅      	| Dropdown box with login form is displayed.                       	|                	|
| 4 	| Enter phone number of a user that was already registered. 	| ✅      	| The user's phone was entered and recognized without errors.      	|                	|
| 5 	| Enter password of a user that was already registered.     	| ✅      	| The user's password was entered and displayed in encrypted form. 	|                	|
| 6 	| Click to the button "Sign in" under the form.             	| ✅      	| User is successfully logged in.                                  	|                	|
| 7 	| Go the cart and check products in the cart.               	| ⛔️      | Cart have two previously selected pizzas.                        	| Cart is empty. 	|


## Test 2 - Correct registration

| Test tour           	| SU-1                       	|
|---------------------	|----------------------------	|
| Object to be tested 	| Sign up using correct data 	|
| Test duration       	| 30 secs                    	|
| Tester              	| Polina Minina              	|

### Protocol

| #  	| Steps                                                                                                                         	| Status 	| Expected result                                                                                                                                                                         	| Actual result                   	|
|----	|-------------------------------------------------------------------------------------------------------------------------------	|--------	|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|---------------------------------	|
| 1  	| Go to main page as unregistered user.                                                                                         	| ✅      	| Main page is opened and user displayed are unregistered.                                                                                                                                	|                                 	|
| 3  	| Click to the button "SIGN IN".                                                                                                	| ✅      	| Dropdown box with login form is displayed.                                                                                                                                              	|                                 	|
| 4  	| Click to the link "Sign up" under the form.                                                                                   	| ✅      	| Dropdown box with register form is displayed.                                                                                                                                           	|                                 	|
| 5  	| Enter user name consisting of numbers in the field "Name".                                                                    	| 💡      	| Display a message about the name being formatted incorrectly because the name must contain letters? Or accept any string format, since the user can identify himself with any nickname? 	| Name is accepted as correct.    	|
| 6  	| Enter phone number with correct city code in the field "Telephone". Example is "+79501234567".                                	| ✅      	| Phone number is successfully accepted.                                                                                                                                                  	|                                 	|
| 7  	| Enter email matching the pattern of usual email addresses but having domain that does not exist. Example is "1234567@а.егше". 	| 💡      	| Accept an email address, because user can create their own unique domain? Reject an email because it has a non-existent domain?                                                         	| Email is successfully accepted. 	|
| 8  	| Enter button "Continue".                                                                                                      	| ✅      	| All entered data is accepted, and new form with password creation is displayed.                                                                                                         	|                                 	|
| 9  	| Create and enter password that matches required rules in the forms "Password" and "Repeat password".                          	| ✅      	| Password is successfully accepted.                                                                                                                                                      	|                                 	|
| 10 	| Enter button "Continue".                                                                                                      	| ✅      	| New user is successfully created, and user info page is opened.                                                                                                                         	|                                 	|

## Test 3 - Incorrect registration

| Test tour           	| SU-2                       	|
|---------------------	|----------------------------	|
| Object to be tested 	| Sign up using incorrect data 	|
| Test duration       	| 20 secs                    	|
| Tester              	| Polina Minina              	|

### Protocol

| # 	| Steps                                                                                                     	| Status 	| Expected result                                                                                                                                                                         	| Actual result                	|
|---	|-----------------------------------------------------------------------------------------------------------	|--------	|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|------------------------------	|
| 1 	| Go to main page as unregistered user.                                                                     	| ✅      	| Main page is opened and user displayed are unregistered.                                                                                                                                	|                              	|
| 3 	| Click to the button "SIGN IN".                                                                            	| ✅      	| Dropdown box with login form is displayed.                                                                                                                                              	|                              	|
| 4 	| Click to the link "Sign up" under the form.                                                               	| ✅      	| Dropdown box with register form is displayed.                                                                                                                                           	|                              	|
| 5 	| Enter user name consisting of numbers in the field "Name".                                                	| 💡      	| Display a message about the name being formatted incorrectly because the name must contain letters? Or accept any string format, since the user can identify himself with any nickname? 	| Name is accepted as correct. 	|
| 6 	| Enter any phone number that have incorrect city code in the field "Telephone". Example is "+71234567890". 	| ✅      	| Phone number is not accepted as valid. Error message is displayed.                                                                                                                      	|                              	|
| 7 	| Enter email consisting of only numbers in the field "Email". Example is "1234567".                        	| ✅      	| Email is not accepted and error message is displayed.                                                                                                                                   	|                              	|
| 8 	| Enter button "Continue".                                                                                  	| ✅      	| Entered data is not accepted, and nothing happens when clicking on button.                                                                                                              	|                              	|


## Test 4 - Registered user card

| Test tour           	| CART-2                       	|
|---------------------	|----------------------------	|
| Object to be tested 	| Creating cart and order    	|
| Test duration       	| 50 secs                    	|
| Tester              	| Polina Minina              	|

### Protocol

| #  	| Steps                                                                                                       	| Status 	| Expected result                                                                                                                                                             	| Actual result                                                                                                             	|
|----	|-------------------------------------------------------------------------------------------------------------	|--------	|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|---------------------------------------------------------------------------------------------------------------------------	|
| 1  	| Go to main page and add three different pizza to the cart.                                                  	| ✅      	| Main page is opened and five pizza is inside cart.                                                                                                                          	|                                                                                                                           	|
| 3  	| Click to the button "Order now" at the bottom of a cart.                                                    	| ✅      	| New page with order details is opened.                                                                                                                                      	|                                                                                                                           	|
| 4  	| Click on the plus sign ("+") in front of one of the pizzas to add two pizzas of the same type to the order. 	| ✅      	| The number of pizzas of this type has been increased by one, that is, now there are 2 of them. The total amount of the order has been increased by the cost of added pizza. 	|                                                                                                                           	|
| 5  	| Click on the cross sign next to one of the pizzas to remove it from the order.                              	| ✅      	| The pizza is removed from the list and the total order amount is reduced by the cost of the removed pizza.                                                                  	|                                                                                                                           	|
| 6  	| After products list in the "Taste it" section, add one of the products to cart.                             	| ❌      	| The selected product is added to the end of the list and the sequence (order) of the products in the cart does not change.                                                  	| The selected product is added to a random place in the list and the sequence (order) of products in the cart has changed. 	|
| 7  	| Select the delivery method "Carry out" and select the address of the restaurant on the map.                 	| ✅      	| In the section "Carry out" the full address and opening hours of the selected restaurant are displayed.                                                                     	|                                                                                                                           	|
| 8  	| In section "Pick up time" select option "As soon as possible".                                              	| ✅      	| Option "As soon as possible" is selected and colored in green.                                                                                                              	|                                                                                                                           	|
| 9  	| In section "Payment" select option "Cash".                                                                  	| ✅      	| Option "Cash" is selected and colored in green.                                                                                                                             	|                                                                                                                           	|
| 10 	| Click on button "Order" to confirm the order in the restaurant.                                             	| ✅      	| New order is successfully created.                                                                                                                                          	|                                                                                                                           	|

# Selenium testing

In the script `tests/su-2.py` there is automation of Test 3 named Incorrect registration.
