from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())

url = 'https://papajohns.ru'
driver.get(url)

# Call menu
driver.find_elements(By.XPATH, "//*[@id='Header']/div/button")[-1].click()
# Click sign in button
driver.find_element(By.XPATH, "//*[@id='Header']/div/div/button").click()
# Click sign up button
reg = driver.find_element(By.XPATH, "//*[@id='Header']/div/div/div/form/div/button").click()

# Fill fields of form
driver.find_element(By.XPATH, "//*[@id='Header']/div/div/div/form/div[1]/div[2]/div/div/input").send_keys("1234567890")
driver.find_element(By.XPATH, "//*[@id='email']").send_keys("1234567")
driver.find_element(By.XPATH, "//*[@id='username']").send_keys("1234567890")

# Check error message
phone_error = driver.find_element(By.XPATH, "//*[@id='Header']/div/div/div/form/div[1]/div[2]/div[2]").text
assert "Некорректный номер телефона" in phone_error

# Check error message
email_error = driver.find_element(By.XPATH, "//*[@id='Header']/div/div/div/form/div[1]/div[3]/div[2]").text
assert "Некорректный email" in email_error

# Click continue button
driver.find_element(By.XPATH, "//*[@id='Header']/div/div/div/form/div[3]/button").click()

# Check that nothing changes in the form after clicking continue
name = driver.find_element(By.XPATH, "//*[@id='username']")
assert name.is_displayed()

driver.close()
